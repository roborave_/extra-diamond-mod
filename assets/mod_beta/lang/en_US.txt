﻿# EDM's Language File = UNITED STATES ENGLISH

# ITEMS
item.Item.Black.name=Carpenter's Hammer
item.Item.Blue.name=Carpenter's Chisel
item.Item.Gold.name=Carpenter's Door


# BLOCKS
tile.Block.Black.name=Black Diamond Block
tile.Block.Blue.name=Blue Diamond Block
tile.Block.Gold.name=Gold Diamond Block
tile.Block.Grey.name=Grey Diamond Block
tile.Block.Orange.name=Orange Diamond Block
tile.Block.Green.name=Green Diamond Block
# </file>