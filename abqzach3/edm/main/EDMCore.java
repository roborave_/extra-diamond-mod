package abqzach3.edm.main;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import abqzach3.edm.main.client.ClientProxy;
import cpw.mods.fml.common.*;
import cpw.mods.fml.common.Mod.*;
import cpw.mods.fml.common.event.*;

@Mod(modid = "EDM_NO_API",name="EDM:Core",version="6.0.0",dependencies="required-after:Forge@[9.11.1.953,)")
public class EDMCore
{
	
	@Instance("EDMCore")
	public EDMCore instance;
	
	@SidedProxy(clientSide="abqzach3.edm.main.client.ClientProxy")
	public static ClientProxy proxy;
	
	@EventHandler
	public static void preload(FMLPreInitializationEvent event)
	{
		proxy.Load();
		
	}
	
	@EventHandler
	public static void load(FMLInitializationEvent event)
	{
		
		
		
	}
	
	@EventHandler
	public static void postload(FMLPostInitializationEvent event)
	{
		
	}
}
