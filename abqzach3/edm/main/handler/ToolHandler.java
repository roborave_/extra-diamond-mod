package abqzach3.edm.main.handler;

import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import abqzach3.edm.main.handler.enums.EnumToolBlack;
import abqzach3.edm.main.items.tools.axe.ItemAxeBlack;
import abqzach3.edm.main.items.tools.pickaxe.ItemPickaxeBlack;

public class ToolHandler {

	public static Item Black_Pickaxe;
	public static Item Black_Axe;
	public static void handleTools()
	{
		Black_Pickaxe = new ItemPickaxeBlack(17000, EnumToolBlack.Black).setCreativeTab(CreativeTabs.tabTools).setUnlocalizedName("Item_Pick_Bla").setTextureName("mod_beta:"+"Black_Diamond_Pick");
		Black_Axe= new ItemAxeBlack(17001,EnumToolBlack.Black).setUnlocalizedName("Item_Axe_Bla");
		
		LanguageRegistry.addName(Black_Pickaxe, "Black Diamond Pickaxe");
		LanguageRegistry.addName(Black_Axe, "Black Diamond Axe");
	}
}
