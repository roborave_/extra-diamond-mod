package abqzach3.edm.main.client;

import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.Loader;
import abqzach3.edm.main.api.BTMAPI;
import abqzach3.edm.main.handler.*;

public class ClientProxy 
{

	public static void Load()
	{
		HandleBlock();
		HandleItem();
		HandleAddons();
		HandleTools();
		
	}
	public static void HandleAddons()
	{
		if(Loader.isModLoaded("BetterTable")) ClientProxy.HandleBTM();
	}
	public static void HandleBlock()
	{
		BlockHandler.RegisterBlocks();
		BlockHandler.BlockToGame();
	}
	
	public static void HandleItem()
	{
		ItemHandler.RegisterItems();
		ItemHandler.ItemToGame();
	}
	public static void HandleBTM(){
		if(BTMAPI.getEWInstalled())
		{
		        BTMAPI.addRecipe(new ItemStack(Block.bedrock, 2), new Object[] {"X", "X", "X", "X", ('X'), Block.cobblestone});
		}
		else
		{
		        System.out.println("This mod requires ExtendedWorkbench.");
		}
	}
	public static void HandleTools(){
		ToolHandler.handleTools();
	}
}
