package abqzach3.edm.main.items.tools.shovel;

import abqzach3.edm.main.handler.enums.EnumToolBlack;
import abqzach3.edm.main.items.tools.ItemToolBlack;
import net.minecraft.block.Block;

public class ItemSpadeBlack extends ItemToolBlack
{

	private static Block[] blocksEffectiveAgainst = new Block[] 
	{
		Block.grass, 
		Block.dirt, 
		Block.sand, 
		Block.gravel, 
		Block.snow, 
		Block.blockSnow, 
		Block.blockClay, 
		Block.tilledField, 
		Block.slowSand, 
		Block.mycelium
	};
	
	public ItemSpadeBlack(int par1, EnumToolBlack par2EnumToolMaterial)
	{
         super(par1, 1, par2EnumToolMaterial, blocksEffectiveAgainst);
	}
	
	public boolean canHarvestBlock(Block par1Block)
	{
         return par1Block == Block.snow ? true : par1Block == Block.blockSnow;
	}
}